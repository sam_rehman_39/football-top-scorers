import { AxiosRequestConfig } from 'axios';
import React, { useEffect } from 'react';
import config from '../../config';
import axios from "axios";
import { FootballAPI } from '../../Definitions/FootballAPI';
import { Scorer } from '../../Definitions/Scorer';
import ScorerCard from '../ScorerCard';

interface Props {
    competitionId: string;
}

function Scorers(props: Props) {
    const [scorers, setScorers] = React.useState<Scorer[] | undefined>(undefined);
    const [fetching, setFetching] = React.useState<boolean>(false);

    useEffect(() => {
        if (!scorers) {
            setFetching(true);
            const { competitionId } = props;
            const { API_URL, API_KEY, AREA, PLAN } = config;
            const requestUrl = `${API_URL}competitions/${competitionId}/scorers`;
            const requestConfig: AxiosRequestConfig = {
                headers: { 'X-Auth-Token': API_KEY },
                params: { areas: AREA, plan: PLAN }
            }
            axios.get(requestUrl, requestConfig)
                .then((res: FootballAPI.ScorersResponse) => {
                    setScorers(res.data.scorers || []);
                    setFetching(false);
                })
                .catch((err) => {
                    setScorers([]);
                    setFetching(false);
                })
        }
    })

    return (
        <div>
            {scorers && scorers.map(({ numberOfGoals, player, team }: Scorer) =>
                <ScorerCard
                    key={player.id}
                    name={player.name}
                    numberOfGoals={numberOfGoals}
                    team={team.name}
                    nationality={player.nationality}
                />)}

            {!fetching && scorers && !scorers.length && <span>No top scorers could be found.</span>}
        </div>
    );
}

export default Scorers;
