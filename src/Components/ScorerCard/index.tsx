import React from 'react';
import styled from "styled-components";

const CardWrapper = styled.div`
display: flex;
align-items: center;
justify-content: space-between;
border-bottom: 0.5px solid grey;
padding-bottom: 20px;
margin: 10px 0;
`

interface Props {
    name: string;
    team: string;
    nationality: string;
    numberOfGoals: number;
}

function ScorerCard(props: Props) {
    const { name, team, nationality, numberOfGoals } = props;
    return (
        <CardWrapper>
            <div>
                <h3>{name}</h3>
                <p>Club: {team}</p>
                <p>Nationality: {nationality}</p>
            </div>
            <div>
                <h3>{numberOfGoals}</h3>
            </div>
        </CardWrapper>
    );
}

export default ScorerCard;
