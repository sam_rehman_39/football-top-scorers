import { Competition } from "../../Definitions/Competition";

export const getRegionsFromCompetitions = (competitions: Competition[]): string[] => {
    const allRegions = competitions.map((competition) => competition.area.name);
    const uniqueRegions = allRegions.filter((region, index) => allRegions.indexOf(region) === index);
    return uniqueRegions;
}

export const filterCompetitions = (region: string, competitions?: Competition[]) => {
    if (!region) {
        return competitions;
    }
    const filteredCompetitions = competitions && competitions.filter((competition) => competition.area.name === region);
    return filteredCompetitions;
}