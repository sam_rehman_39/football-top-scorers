import * as H from 'history';
import React, { useEffect } from 'react';
import axios, { AxiosRequestConfig } from "axios";
import config from "../../config";
import { FootballAPI } from '../../Definitions/FootballAPI';
import { Competition } from '../../Definitions/Competition';
import { filterCompetitions, getRegionsFromCompetitions } from './logic';
import styled from "styled-components";

const Select = styled.select`
padding: 15px;
border-radius: 5px;
`

const FilterContainer = styled.div`
display: flex;
align-items: center;
justify-content: space-between;
margin: 20px 0;
`

const Button = styled.button`
  padding: 15px;
  background: darkcyan;
  color: white;
  border: none;
  border-radius: 5px;
  cursor: pointer;
  min-width: 130px;
  `

const CompetitionContainer = styled.div`
display: flex;
align-items: center;
justify-content: space-between;
border-bottom: 0.5px solid grey;
padding-bottom: 20px;
margin: 10px 0;
`

interface Props {
  history: H.History;
}

function Competitions(props: Props) {
  const [competitions, setCompetitions] = React.useState<Competition[] | undefined>(undefined);
  const [displayedCompetitions, setDisplayedCompetitions] = React.useState<Competition[] | undefined>(undefined);
  const [regions, setRegions] = React.useState<String[] | undefined>(undefined);
  const [fetching, setFetching] = React.useState<boolean>(false);

  useEffect(() => {
    if (!competitions) {
      setFetching(true);
      const { API_URL, API_KEY, AREA, PLAN } = config;
      const requestUrl = `${API_URL}competitions`;
      const requestConfig: AxiosRequestConfig = {
        headers: { 'X-Auth-Token': API_KEY },
        params: { areas: AREA, plan: PLAN }
      }
      axios.get(requestUrl, requestConfig)
        .then((res: FootballAPI.CompetitionsResponse) => {
          const regions: string[] = getRegionsFromCompetitions(res.data.competitions);
          setCompetitions(res.data.competitions || []);
          setDisplayedCompetitions(res.data.competitions || []);
          setRegions(regions);
          setFetching(false);
        })
        .catch((err) => {
          setCompetitions([]);
          setDisplayedCompetitions(undefined);
          setRegions(undefined);
          setFetching(false);
        })
    }
  })

  const handleChange = (event: React.ChangeEvent<HTMLSelectElement>) => {
    const filteredCompetitions = filterCompetitions(event.target.value, competitions);
    setDisplayedCompetitions(filteredCompetitions);
  }

  const viewScorers = (id: number) => {
    const { history, } = props;
    history.push(`/scorers/${id}`)
  }

  return (
    <div>
      <FilterContainer>
        <div>
          <span>Filter by region: </span>
        </div>
        <Select onChange={handleChange}>
          <option value="">Select region</option>
          {regions && regions.map((region, index) => <option key={index}>{region}</option>)}
        </Select>
      </FilterContainer>

      {displayedCompetitions && displayedCompetitions.map(({ id, name, area }: Competition) =>
        <CompetitionContainer key={id}>
          <div role="competition">
            <span>
              <h4>{name}</h4>
              {area.name}
            </span>
          </div>
          <Button onClick={() => viewScorers(id)}>View top scorers</Button>
        </CompetitionContainer>
      )}

      {!fetching && !displayedCompetitions && <div role="no-results"><span>No competitions could be found.</span></div>}
    </div>
  );
}

export default Competitions;
