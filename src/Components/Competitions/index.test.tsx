
import * as H from 'history';
import React from 'react'
import { rest } from 'msw'
import { setupServer } from 'msw/node'
import { render, waitFor, screen, fireEvent } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import Competitions from '.'
import config from '../../config'

const { API_URL } = config;
const requestUrl = `${API_URL}competitions`;
const server = setupServer(
    rest.get(requestUrl, (req, res, ctx) => {
        return res(ctx.json({
            data: {
                count: 1,
                competitions: [
                    { id: 1, name: "BL", area: { name: "Germany", id: 1, countryCode: "GER" } }
                ]
            }
        }))
    })
)

beforeAll(() => server.listen())
afterEach(() => server.resetHandlers())
afterAll(() => server.close())

test('loads and displays competition', async () => {
    render(<Competitions history={{} as H.History} />)

    await screen.findByRole("competition")

    expect(screen.getByRole('competition')).toHaveTextContent('BL');
})

test('handles server error', async () => {
    server.use(
        rest.get(requestUrl, (req, res, ctx) => {
            return res(ctx.status(500))
        })
    )

    render(<Competitions history={{} as H.History} />)

    await waitFor(() => screen.getByRole('no-results'))

    expect(screen.getByRole('no-results')).toHaveTextContent('No competitions could be found.')
})