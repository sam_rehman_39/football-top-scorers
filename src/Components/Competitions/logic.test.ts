import { Competition } from "../../Definitions/Competition";
import { getRegionsFromCompetitions, filterCompetitions } from "./logic";

const mockCompetitions: Competition[] = [
    { id: 0, name: "PL", area: { name: "England", id: 0, countryCode: "ENG" } },
    { id: 1, name: "BL", area: { name: "Germany", id: 1, countryCode: "GER" } }
]

describe("Rendering logic in Competitions.tsx", () => {

    test("regions are extracted correctly from competitions", () => {
        const expectedValue = ["England", "Germany"];
        expect(getRegionsFromCompetitions(mockCompetitions)).toEqual(expectedValue)
    })

    describe("competitions are filtered correctly", () => {

        test("successful filter application", () => {
            const expectedValue = [{ id: 0, name: "PL", area: { name: "England", id: 0, countryCode: "ENG" } }]
            expect(filterCompetitions("England", mockCompetitions)).toEqual(expectedValue);
        })

        test("default to all competitions when region value is empty", () => {
            expect(filterCompetitions("", mockCompetitions)).toEqual(mockCompetitions);
        })
    })

})