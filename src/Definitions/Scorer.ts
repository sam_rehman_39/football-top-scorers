interface Player {
    id: number;
    name: string;
    nationality: string;
    position: string;
}

interface Team {
    id: number;
    name: string;
}

export interface Scorer {
    numberOfGoals: number;
    player: Player;
    team: Team;
}