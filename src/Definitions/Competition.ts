interface Area {
    id: number;
    countryCode: string;
    name: string;
}

export interface Competition {
    id: number;
    area: Area;
    name: string;
}