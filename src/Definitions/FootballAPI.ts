import { Competition } from "./Competition";
import { Scorer } from "./Scorer";

export interface SearchResult {
    id: string;
    address: string;
}


export namespace FootballAPI {

    export interface CompetitionsResponse {
        data: {
            count: number;
            competitions: Competition[];
        };
    }

    export interface ScorersResponse {
        data: {
            count: number;
            scorers: Scorer[]
        }
    }
}
