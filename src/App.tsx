import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from "react-router-dom";
import Competitions from './Components/Competitions/index';
import Scorers from './Components/Scorers';
import styled from "styled-components"


const Wrapper = styled.div`
font-family: sans-serif;
margin: 20px auto;
max-width: 650px;
`;

function App() {
  return (
    <BrowserRouter>
      <main className="App_content">
        <Wrapper>
          <Switch>
            <Route path="/scorers/:competitionId">
              {(props) => (
                <Scorers competitionId={props.match?.params.competitionId!} />
              )}
            </Route>
            <Route path="/competitions">
              {(props) => (
                <Competitions history={props.history} />
              )}
            </Route>
            <Redirect to="/competitions" />
          </Switch>
        </Wrapper>
      </main>
    </BrowserRouter>
  );
}

export default App;
